import React from 'react';
import Card from "../components/Card";
import Button from "../components/Button";
import Dropdown from "../components/Dropdown";
import Item from "../components/Item";
import Pagination from "../components/Pagination";
import List from "../components/List";
import MyModal from "../components/MyModal";

export default function Example() {
    return (

    <div className="mx-5">
        <div className="flex justify-between">
            <h3 className="py-5"> Analytics </h3>
            <div className="flex space-x-4 py-5 w-1/4">
                <MyModal/>
                <Button
                    classes={'text-xs p-2 rounded bg-indigo-400 w-5/6'}
                    content = {'Alt info'}
                />
            </div>
        </div>

        <div className="flex space-x-4 pb-5 ">

         <Card
             cardTitle={'ceva scris frumos'}
             cardInfo={'informatii'}
         />
         <Card
             cardTitle={'alt scris frumos'}
             cardInfo={'alte informatii'}
         />

        </div>

        <div className="w-full space-x-4 flex">
            <div className="w-1/3 flex flex-col">
                <div className=" justify-between flex pb-2">
                    <h3 className="my-auto"> Country</h3>
                    <Dropdown/>
                </div>
                <div className="justify-between flex flex-col">
                    <List/>
                        <div className="flex flex-col">
                            <Pagination/>
                        </div>
                </div>

            </div>

            <div className="w-2/3">
                <Item/>
            </div>
        </div>
    </div>
    )
}