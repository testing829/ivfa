

import React from 'react';

export default function List() {
    return (
        <div className="flex justify-center">
            <ul className="bg-white rounded-lg border border-gray-200 w-96 text-gray-900">
                <li className="px-6 py-2 border-b border-gray-200 w-full rounded-t-lg bg-blue-600 text-white">First Applicant</li>
                <li className="px-6 py-2 border-b border-gray-200 w-full">Second Applicant</li>
                <li className="px-6 py-2 border-b border-gray-200 w-full">Third Applicant</li>
                <li className="px-6 py-2 w-full rounded-b-lg">Fourth Applicant</li>
            </ul>
        </div>
    )
}