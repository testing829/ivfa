import React from 'react';
import icon from '../components/images/wires-wire.svg'

export default function Card({cardTitle, cardInfo}) {
    return (
        <div className="flex w-full bg-indigo-600 rounded">
            <div className="w-8 my-auto m-2">
                <img src={icon}/>
            </div>
            <div className="divide-y w-3/4 text-white">
                <h3 className="my-2"> {cardTitle} </h3>
                <p className="my-2">{cardInfo}</p>
            </div>
        </div>
    )
}