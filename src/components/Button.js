import React from 'react';

export default function Button({toggle, content, classes}) {



    return (
                <button
                    className={classes}
                    onClick={toggle}
                >
                    {content}
                </button>
    )
}